from selenium import webdriver
from selenium.webdriver.chrome.service import Service

options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-logging'])
options.add_argument('headless')
webdriverPath = "C:/Program Files/ChromeDriver/chromedriver.exe"
service = Service(webdriverPath)
driver = webdriver.Chrome(service=service, options=options)

page_url = 'https://www.nationalgeographic.com/'
driver.get(page_url)
try:
    qwe = driver.find_element('xpath', '//*[@class="GeneralHeading__Header GeneralHeading__Header--caps"]').text
    print(qwe)
except Exception as e:
    print(e)
    driver.close()
    driver.quit()
    exit(1)
driver.close()
driver.quit()
exit(0)